<?php

namespace App\Mail;

use Illuminate\Bus\Queueable;
use Illuminate\Contracts\Queue\ShouldQueue;
use Illuminate\Mail\Mailable;
use Illuminate\Queue\SerializesModels;

use App\OtpCode;

class OtpCodeMail extends Mailable
{
    use Queueable, SerializesModels;

    public $otpCode, $newUserStatus;

    /**
     * Create a new message instance.
     *
     * @return void
     */
    public function __construct(OtpCode $otpCode, $newUserStatus = false)
    {
        $this->otpCode = $otpCode;
        $this->newUserStatus = $newUserStatus;
    }

    /**
     * Build the message.
     *
     * @return $this
     */
    public function build()
    {
        return $this->view('mails.otpCode.mail')
            ->subject('FullStack Web');

      
        
    }
}
